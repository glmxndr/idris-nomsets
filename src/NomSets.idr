module NomSets

import public NomSets.Atom
import public NomSets.GSet

%access public export
%default total



data Element : Nat -> Type where
  U0 : Nat -> Element 0
  US : Element n -> Element (S n)
  UU : GSet (Element n) -> Element (S n)



data Permut : Type where
  PermutId : Permut
  PermutSwap : (a:Nat) -> (b:Nat) -> Permut
  PermutComp : Permut -> Permut -> Permut



permutAction : Permut -> Element n -> Element n
permutAction PermutId x = x
permutAction (PermutSwap a b) (U0 c) =
  case a == c of
  True => U0 b
  False => case b == c of
           True => U0 a
           False => U0 c
permutAction p@(PermutSwap a b) (US x) = US (permutAction p x)
permutAction p@(PermutSwap a b) (UU f elems) = ?h_5
permutAction (PermutComp p p') x = ?h_3


