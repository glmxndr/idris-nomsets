module WTypes

%default total

data W :  (A : Type)
       -> (B : A -> Type)
       -> Type
where
  Sup :  {A : Type}
      -> {B : A -> Type}
      -> (a : A)
      -> (B a -> W A B)
      -> W A B

{-
welim :  {A : Type}
     -> {B : A -> Type}
     -> {C : W A B -> Type}
     -> (w : W A B)
     -> (a : A)
     -> (f : B a -> W A B)
     -> (c : (b : B a) -> C (f b))
     -> C (Sup a f)
welim = ?h
-}

-- ===== WNat ======

wnat : Bool -> Type
wnat True = Unit
wnat False = Void

WNat : Type
WNat = W Bool wnat

wNatS : WNat -> WNat
wNatS n = Sup True (\unit => n)

wNatZ : WNat
wNatZ = Sup False absurd

w2 : WNat
w2 = wNatS $ wNatS wNatZ

wnatElim :  {C : WNat -> Type}
         -> (n : WNat)
         -> (b : Bool)
         -> (f : wnat b -> WNat)
         -> (c : (m : wnat b) -> C (f m))
         -> C (Sup b f)
wnatElim = ?h


-- ===== WList =====

wlist : Maybe a -> Type
wlist (Just _) = Unit
wlist _ = Void

WList : Type -> Type
WList a = W (Maybe a) wlist

wCons : a -> WList a -> WList a
wCons a rest = Sup (Just a) (\unit => rest)

wNil : WList a
wNil = Sup Nothing absurd

-- ===== WBTree =====

wbtree : Either b a -> Type
wbtree (Right _) = Bool
wbtree _ = Void

WBTree : Type -> Type -> Type
WBTree a b = W (Either b a) wbtree

wBLeaf : b -> WBTree a b
wBLeaf b = Sup (Left b) absurd

wBNode : a -> WBTree a b -> WBTree a b -> WBTree a b
wBNode x l r = Sup (Right x) (\b => case b of True => l; False => r)


