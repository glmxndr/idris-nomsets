module NomSets.GNESet

import Data.Vect

import public NomSets.WOrd
import public NomSets.WOrd.Lemmas

%access public export
%default total

-- Modelling sets as ordered list
-- Non empty tagged sets, the tag is the lowest element
data GNESet : (t:Type) -> t -> Type where
  Single : (x:t) -> GNESet t x
  Cons   : (w:WOrd t) => {y:t}
                      ->(x:t)
                      -> (!<<!) @{w} x y
                      -> GNESet t y
                      -> GNESet t x

WOrd t => Uninhabited (the (GNESet t a) (Single a) = the (GNESet t b) (Cons b _ _)) where
 uninhabited Refl impossible

WOrd t => Uninhabited (the (GNESet t a) (Cons a _ _) = the (GNESet t b) (Single b)) where
 uninhabited Refl impossible

--========== START LEMMAS

lower_bound : GNESet t a -> t
lower_bound (Single n) = n
lower_bound (Cons n _ _) = n



dec_same_lower_bound : DecEq t =>
                       GNESet t a
                     -> GNESet t b
                     -> Dec (a = b)
dec_same_lower_bound {a} {b} _ _ = decEq a b



set_index_is_lower_bound : (s:GNESet t a)
                         -> a = lower_bound s
set_index_is_lower_bound (Single _) = Refl
set_index_is_lower_bound (Cons _ _ _) = Refl



eq_single_destruct : WOrd t =>
                     the (GNESet t a ) (Single a)
                   = the (GNESet t b) (Single b)
                   -> a = b
eq_single_destruct Refl = Refl



eq_rest_cons_destruct : WOrd t =>
                        the (GNESet t a) (Cons a ltax set_a)
                      = the (GNESet t a) (Cons a ltax set_b)
                      -> set_a = set_b
eq_rest_cons_destruct Refl = Refl



eq_rest_cons_destruct' : WOrd t =>
                        the (GNESet t a) (Cons a ltab set_b)
                      = the (GNESet t a) (Cons a ltac set_c)
                      -> set_b = set_c
eq_rest_cons_destruct' Refl = Refl



eq_v_cons_destruct : WOrd t =>
                        the (GNESet t a) (Cons a ltab set_b)
                      = the (GNESet t c) (Cons c ltcd set_d)
                      -> a = c
eq_v_cons_destruct Refl = Refl



eq_prf_cons_destruct : WOrd t =>
                       the (GNESet t a) (Cons a ltab set_b)
                     = the (GNESet t c) (Cons c ltcd set_d)
                     -> ltab = ltcd
eq_prf_cons_destruct Refl = Refl

eqab_setb_implies_seta : a = b
                       -> GNESet t b
                       -> GNESet t a
eqab_setb_implies_seta eqab set_a = rewrite eqab in set_a



decEqCombo : WOrd t => (a:t) -> (c:t)
           -> (ltab : a !<<! b)
           -> (ltcd : c !<<! d)
           -> Dec (ltab = ltcd)
decEqCombo a c ltab ltcd = decEqH {c=c} ltab ltcd



highend : WOrd t => {a,b : t}
        -> a!<<!b
        -> t
highend {b} _ = b



gnesetDecEqH : WOrd t => (sa : GNESet t a)
             -> (sc : GNESet t c)
             -> Dec (sa = sc)
gnesetDecEqH (Single a) (Single c) with (decEq a c)
  gnesetDecEqH (Single a) (Single a) | Yes Refl = Yes Refl
  gnesetDecEqH (Single a) (Single c) | No contra = No (contra . eq_single_destruct)
gnesetDecEqH (Single _) (Cons _ _ _) = No absurd
gnesetDecEqH (Cons _ _ _) (Single _) = No absurd
gnesetDecEqH (Cons a ltab set_b) (Cons c ltcd set_d) with (decEq a c)
  gnesetDecEqH (Cons a ltab set_b) (Cons c ltcd set_d)  | No contra = No (contra . eq_v_cons_destruct)
  gnesetDecEqH (Cons a ltab set_b) (Cons a ltad set_d)  | Yes Refl with (gnesetDecEqH set_b set_d)
    gnesetDecEqH (Cons a ltab set_b) (Cons a ltad set_d) | Yes Refl | No contra = No (contra . believe_me)
    gnesetDecEqH (Cons a ltab set_b) (Cons a ltab' set_b)  | Yes Refl | Yes Refl with (decEqH {a=a} {c=a} ltab ltab')
      gnesetDecEqH (Cons a ltab set_b) (Cons a ltab set_b)  | Yes Refl | Yes Refl | Yes Refl = Yes Refl
      gnesetDecEqH (Cons a ltab set_b) (Cons a ltab' set_b)  | Yes Refl | Yes Refl | No contra = No (contra . eq_prf_cons_destruct)



decEqWOrdImplem :  (w1 : WOrd t)
                -> (w2 : WOrd t)
                -> Dec (w1 = w2)
decEqWOrdImplem w1 w2 = Yes (believe_me "I put a '| t' in WOrd t definition")



--============= END LEMMAS



WOrd t => DecEq (GNESet t n) where
  decEq = gnesetDecEqH

WOrd t => Eq (GNESet t n) where
   x == y with (decEq x y)
     x == x | Yes Refl = True
     x == y | No _ = False



add : (w:WOrd t) => (a:t)
                 -> GNESet t b
                 -> GNESet t (wmin a b)

add   @{w} a (Single b) with (total_order @{w} a b)
  add @{w} a (Single b) | Red ltab = Cons @{w} a ltab (Single b)
  add @{w} a (Single a) | Grn Refl = Single a
  add @{w} a (Single b) | Blu ltba = Cons @{w} b ltba (Single a)

add       @{w} a (Cons {y=c} @{w1} b ltbc set_c) with (decEqWOrdImplem w w1)
  add     @{w} a (Cons {y=c} @{w1} b ltbc set_c) | No contra = believe_me "It can't happen"
  add     @{w} a (Cons {y=c} @{w}  b ltbc set_c) | Yes Refl with (total_order @{w} a b)
    add   @{w} a (Cons {y=c} @{w}  b ltbc set_c) | Yes Refl | Red ltab = Cons @{w} a ltab (Cons b ltbc set_c)
    add   @{w} a (Cons {y=c} @{w}  a ltac set_c) | Yes Refl | Grn Refl = Cons a ltac set_c
    add   @{w} a (Cons {y=c} @{w}  b ltbc set_c) | Yes Refl | Blu ltba with (total_order @{w} a c)
      add @{w} a (Cons {y=c} @{w}  b ltbc set_c) | Yes Refl | Blu ltba | Red ltac =
        Cons @{w} b (ltab_ltac_implies_ltaminbc b a c ltba ltbc) (add @{w} a set_c)
      add @{w} a (Cons {y=a} @{w}  b ltba set_a) | Yes Refl | Blu _    | Grn Refl =
        Cons @{w} b ltba set_a
      add @{w} a (Cons {y=c} @{w}  b ltbc set_c) | Yes Refl | Blu ltba | Blu ltca =
        Cons @{w} b (ltab_ltac_implies_ltaminbc b a c ltba ltbc) (add @{w} a set_c)



merge : (w:WOrd t) => GNESet t a
                   -> GNESet t b
                   -> GNESet t (wmin a b)

merge @{w} (Single a) set_b = add @{w} a set_b

merge   @{w} (Cons {y=c} @{x} a ltac set_c) (Single b) with (decEqWOrdImplem w x)
  merge @{w} (Cons {y=c} @{x} a ltac set_c) (Single b) | No contra = believe_me "impossible"
  merge @{w} (Cons {y=c} @{w} a ltac set_c) (Single b) | Yes Refl  =
    rewrite wmin_symm @{w} a b in add @{w} b (Cons @{w} a ltac set_c)

merge       @{w} (Cons {y=c} @{x} a ltac set_c) (Cons {y=d} @{y} b ltbd set_d) with (decEqWOrdImplem w x)
  merge     @{w} (Cons {y=c} @{x} a ltac set_c) (Cons {y=d} @{y} b ltbd set_d) | No contra = believe_me "impossible"
  merge     @{w} (Cons {y=c} @{w} a ltac set_c) (Cons {y=d} @{y} b ltbd set_d) | Yes Refl with (decEqWOrdImplem w y)
    merge   @{w} (Cons {y=c} @{w} a ltac set_c) (Cons {y=d} @{y} b ltbd set_d) | Yes Refl | No contra = believe_me "impossible"
    merge   @{w} (Cons {y=c} @{w} a ltac set_c) (Cons {y=d} @{w} b ltbd set_d) | Yes Refl | Yes Refl with (total_order @{w} a b)
      merge @{w} (Cons {y=c} @{w} a ltac set_c) (Cons {y=d} @{w} b ltbd set_d) | Yes Refl | Yes Refl | Red ltab =
        rewrite ltab_ltac_implies_a_is_minaminbc a c b ltac ltab in add a (merge set_c (Cons {y=d} b ltbd set_d))
      merge @{w} (Cons {y=c} @{w} a ltac set_c) (Cons {y=d} @{w} a ltad set_d) | Yes Refl | Yes Refl | Grn Refl =
        rewrite ltab_ltac_implies_a_is_minaminbc a c d ltac ltad in add a (merge set_c set_d)
      merge @{w} (Cons {y=c} @{w} a ltac set_c) (Cons {y=d} @{w} b ltbd set_d) | Yes Refl | Yes Refl | Blu ltba =
        rewrite ltab_ltac_implies_a_is_minaminbc b a d ltba ltbd in add b (merge (Cons {y=c} @{w} a ltac set_c) set_d)



toList : GNESet t a -> List t
toList (Single a) = [a]
toList (Cons a _ set_b) = a :: toList set_b

Cast (GNESet t a) (List t) where
  cast = toList



toVect : GNESet t a -> (n ** Vect (S n) t)
toVect (Single a) = (Z ** [a])
toVect (Cons a _ set_b) = let (n ** vb) = toVect set_b in (S n ** a :: vb)


