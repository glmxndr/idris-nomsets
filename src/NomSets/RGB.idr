module NomSets.RGB

%access public export
%default total

data RGB r g b = Red r | Grn g | Blu b
