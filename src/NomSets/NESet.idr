module NomSets.NESet

import NomSets.Atom
import NomSets.SLT
import NomSets.TotalOrdering

%access public export
%default total

-- Modelling sets as ordered list
-- Non empty tagged sets, the tag is the lowest element
data NESet : Atom -> Type where
  Single : (x:Atom) -> NESet x
  Cons   : (x:Atom)
         -> (prf: x !<! y)
         -> NESet y
         -> NESet x

lower_bound : NESet _ -> Atom
lower_bound (Single n) = n
lower_bound (Cons n _ _) = n

dec_same_lower_bound : NESet a -> NESet b -> Dec (a = b)
dec_same_lower_bound {a} {b} _ _ = decEq a b

set_index_is_lower_bound : (s:NESet a) -> a = lower_bound s
set_index_is_lower_bound (Single _) = Refl
set_index_is_lower_bound (Cons _ _ _) = Refl

Uninhabited (Single a = Cons b _ _) where
  uninhabited Refl impossible
Uninhabited (Cons a _ _ = Single b) where
  uninhabited Refl impossible

eq_single_destruct : Single a = Single b -> a = b
eq_single_destruct Refl = Refl

eq_rest_cons_destruct : (Cons a ltax set_a = Cons a ltax set_b) -> (set_a = set_b)
eq_rest_cons_destruct Refl = Refl

eq_prf_cons_destruct : (Cons a ltab set_b = Cons c ltcd set_d) -> (ltab = ltcd)
eq_prf_cons_destruct Refl = Refl

eqab_setb_implies_seta : a = b -> NESet b -> NESet a
eqab_setb_implies_seta eqab set_a = rewrite eqab in set_a

nesetDecEqH : (s:NESet a) -> (t:NESet b) -> Dec (s = t)
nesetDecEqH (Single a) (Single b) with (decEq a b)
  nesetDecEqH (Single a) (Single a) | Yes Refl = Yes Refl
  nesetDecEqH (Single a) (Single b) | No contra = No (contra . eq_single_destruct)
nesetDecEqH (Single _) (Cons _ _ _) = No absurd
nesetDecEqH (Cons _ _ _) (Single _) = No absurd
nesetDecEqH (Cons a ltab set_b) (Cons c ltcd set_d) with (sltDecEqH ltab ltcd)
  nesetDecEqH (Cons a ltab set_b) (Cons a ltab set_b') | Yes Refl with (nesetDecEqH set_b set_b')
    nesetDecEqH (Cons a ltab set_b) (Cons a ltab set_b)  | Yes Refl | Yes Refl = Yes Refl
    nesetDecEqH (Cons a ltab set_b) (Cons a ltab set_b') | Yes Refl | No contra = No (contra . eq_rest_cons_destruct)
  nesetDecEqH (Cons a ltab set_b) (Cons c ltcd set_d)  | No contra = No (contra . eq_prf_cons_destruct)

DecEq (NESet n) where
  decEq = nesetDecEqH

Eq (NESet n) where
   x == y with (decEq x y)
     x == x | Yes Refl = True
     x == y | No _ = False

nesetCompareH : NESet a -> NESet b -> Ordering
nesetCompareH (Single a) (Single b) = compare a b
nesetCompareH (Single a) (Cons b ltbc set_c) =
    case compare a b of
    LT => LT
    EQ => LT
    GT => GT
nesetCompareH (Cons a ltab set_b) (Single c) =
    case compare a c of
    LT => LT
    EQ => GT
    GT => GT
nesetCompareH (Cons a ltab set_b) (Cons c ltcd set_d) =
    case compare a c of
    LT => LT
    GT => GT
    EQ => nesetCompareH set_b set_d

Ord (NESet n) where
  compare = nesetCompareH

-- ========= LEMMAS

min_n_n_is_n : (n:Nat) -> minimum n n = n
min_n_n_is_n Z = Refl
min_n_n_is_n (S k) = cong $ min_n_n_is_n k

min_symm : (a:Nat) -> (b:Nat) -> minimum a b = minimum b a
min_symm Z Z = Refl
min_symm Z (S k) = Refl
min_symm (S k) Z = Refl
min_symm (S k) (S j) = cong $ min_symm k j

slt_min : a !<! b -> minimum a b = a
slt_min SLTZ = Refl
slt_min (SLTS x) = cong $ slt_min x

slt_min' : a !<! b -> minimum b a = a
slt_min' {a} {b} ltab = rewrite min_symm b a in slt_min ltab

combine_lts : a !<! b -> a !<! c -> a !<! (minimum b c)
combine_lts {b} {c} ltab ltac =
  case tord b c of
  TLT ltbc => rewrite slt_min ltbc in ltab
  TEQ Refl => rewrite min_n_n_is_n c in ltac
  TGT ltcb => rewrite min_symm b c in
              rewrite slt_min ltcb in ltac

ltab_ltac_implies_minab_is_minac : a !<! b -> a !<! c -> minimum a b = minimum a c
ltab_ltac_implies_minab_is_minac ltab ltac =
  rewrite slt_min ltab in rewrite slt_min ltac in Refl

ltab_implies_minaa_is_min_ab : a !<! b -> minimum a a = minimum a b
ltab_implies_minaa_is_min_ab {a} ltab =
  rewrite slt_min ltab in
  rewrite sym $ min_n_n_is_n a in Refl

ltab_ltac_implies_minac_is_minaminbc : a !<! b -> a !<! c -> minimum a c = minimum a (minimum b c)
ltab_ltac_implies_minac_is_minaminbc ltab ltac =
  let lta_minbc = combine_lts ltab ltac in
  ltab_ltac_implies_minab_is_minac ltac lta_minbc

ltab_ltac_implies_minba_is_minaminbc : a !<! b -> a !<! c -> minimum b a = minimum a (minimum b c)
ltab_ltac_implies_minba_is_minaminbc {a} {b} ltab ltac =
  let lta_minbc = combine_lts ltab ltac in
  rewrite min_symm b a in
  ltab_ltac_implies_minab_is_minac ltab lta_minbc


ltab_ltac_implies_minaa_is_minaminbc : a !<! b -> a !<! c -> minimum a a = minimum a (minimum b c)
ltab_ltac_implies_minaa_is_minaminbc ltab ltac =
  rewrite ltab_implies_minaa_is_min_ab ltac in
  ltab_ltac_implies_minab_is_minac ltac (combine_lts ltab ltac)

-- ========= add an atom to an NESet

add : (m:Atom) -> NESet n -> NESet (minimum m n)
add m sn@(Single n) =
  case tord m n of
  TLT ltmn => rewrite slt_min ltmn in Cons m ltmn sn
  TEQ Refl => rewrite min_n_n_is_n n in sn
  TGT ltnm => rewrite min_symm m n in
              rewrite slt_min ltnm in
              Cons n ltnm (Single m)
add m set_n@(Cons n ltno set_o) =
  case tord m n of
  TLT ltmn => rewrite slt_min ltmn in Cons m ltmn set_n
  TEQ Refl => rewrite min_n_n_is_n n in set_n
  TGT ltnm => rewrite min_symm m n in
              rewrite slt_min ltnm in
              Cons n (combine_lts ltnm ltno) (add m set_o)

test_add1_to_set02 :
    add 1 (Cons 0 NomSets.SLT.lt02 (Single 2))
  = (Cons 0 NomSets.SLT.lt01 (Cons 1 NomSets.SLT.lt12 (Single 2)))
test_add1_to_set02 = Refl

-- ========= merge NESets

merge : NESet m -> NESet n -> NESet (minimum m n)
merge (Single m) set_n = add m set_n
merge set_m@(Cons m ltmo set_o) s@(Single n) = rewrite min_symm m n in add n set_m
merge set_m@(Cons m ltmo set_o) set_n@(Cons n ltnp set_p) =
  case tord m n of
  TLT ltmn => rewrite ltab_ltac_implies_minac_is_minaminbc ltmo ltmn in
              add m (merge set_o set_n)
  TEQ Refl => rewrite ltab_ltac_implies_minaa_is_minaminbc ltmo ltnp in
              add m (merge set_o set_p)
  TGT ltnm => rewrite ltab_ltac_implies_minba_is_minaminbc ltnm ltnp in
              add n (merge set_m set_p)

test_merge_set01_set_12_is_set_012 :
    NomSets.NESet.merge
      (Cons 0 NomSets.SLT.lt01
        (Single 1))
      (Cons 1 NomSets.SLT.lt12
        (Single 2))
  = Cons 0 NomSets.SLT.lt01
     (Cons 1 NomSets.SLT.lt12
       (Single 2))
test_merge_set01_set_12_is_set_012 = Refl
