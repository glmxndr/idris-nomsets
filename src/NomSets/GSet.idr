module NomSets.GSet

import public NomSets.GNESet
import public NomSets.GNELT

%access public export
%default total

data GSet : Type -> Type where
  Nil  : GSet t
  Some : WOrd t => GNESet t n -> GSet t

add : WOrd t => t -> GSet t -> GSet t
add n Nil = Some (Single n)
add n (Some s) = Some (add n s)

merge : WOrd t => GSet t -> GSet t -> GSet t
merge Nil x = x
merge (Some x) Nil = Some x
merge (Some x) (Some y) = Some (merge x y)

implementation WOrd t => Semigroup (GSet t) where
  (<+>) = merge
implementation WOrd t => Monoid (GSet t) where
  neutral = Nil

implementation WOrd t => Uninhabited (Nil = Some (the (GNESet t a) set_a)) where
  uninhabited Refl impossible
implementation WOrd t => Uninhabited (Some (the (GNESet t a) set_a) = Nil) where
  uninhabited Refl impossible

eq_some_destruct : WOrd t =>
                 (x : GNESet t a)
                 -> (y : GNESet t b)
                 -> (Some x = Some y)
                 -> x = y
eq_some_destruct _ _ Refl = Refl

implementation WOrd t => DecEq (GSet t) where
  decEq Nil Nil = Yes Refl
  decEq Nil (Some _) = No absurd
  decEq (Some _) Nil = No absurd
  decEq (Some x) (Some y) with (gnesetDecEqH x y)
    decEq (Some x) (Some x) | Yes Refl = Yes Refl
    decEq (Some x) (Some y) | No contra = No (contra . (eq_some_destruct x y))

implementation WOrd t => Eq (GSet t) where
    x == y with (decEq x y)
      x == x | Yes Refl = True
      x == y | No _ = False



infix 4 <:
data (<:) : GSet t -> GSet t -> Type where
  NilBase : (w:WOrd t) => {a:t}
                       -> {set_a : GNESet t a}
                       -> Nil <: Some set_a
  Direct  : (w:WOrd t) => {a,b : t}
                       -> (set_a : GNESet t a)
                       -> (set_b : GNESet t b)
                       -> (ltab : a !<<! b)
                       -> Some set_a <: Some set_b
  Indirect: (wg:WOrd (GNESet t a)) => {auto w : WOrd t}
                                   -> (set1 : GNESet t a)
                                   -> (set2 : GNESet t a)
                                   -> (!<<!) @{wg} set1 set2
                                   -> Some set1 <: Some set2


namespace WOrdGSet

  implementation WOrd t => WOrd (GSet t) where

    (!<<!) = (<:)

    base = Nil

    antisymmetric = wog_as
      where
        wog_as : {set1,set2: GSet t}
               -> set1 <: set2
               -> Not (set2 <: set1)
        wog_as NilBase NilBase impossible
        wog_as NilBase (Direct _ _ _) impossible
        wog_as NilBase (Indirect _ _ _) impossible
        wog_as (Direct {a=a} {b=b} @{w} _ _ ltab) (Direct {a=b} {b=a} @{w} _ _ ltba) = antisymmetric @{w} ltab ltba
        wog_as (Direct {a=a} {b=a} @{w} _ _ ltaa) (Indirect _ _ _) = strict @{w} ltaa
        wog_as (Indirect {a=a} @{w} _ _ _) (Direct {a=a} @{w} _ _ _) impossible --= strict @{w} ltaa
        wog_as (Indirect {w} @{wg1} _ _ lt12) (Indirect {w} @{wg2} _ _ lt21) with (decEqWOrdImplem wg1 wg2)
          wog_as (Indirect {w} @{wg1} _ _ lt12) (Indirect {w} @{wg2} _ _ lt21) | No _ = believe_me ""
          wog_as (Indirect {w} @{wg}  _ _ lt12) (Indirect {w} @{wg}  _ _ lt21) | Yes Refl = antisymmetric @{wg} lt12 lt21


    transitive NilBase (Direct _ _ _) = NilBase
    transitive NilBase (Indirect _ _ _) = NilBase
    transitive (Direct {a=a} @{w} set_a _ ltab) (Direct {a=b} {b=c} @{w} _ set_c ltbc) = Direct {a=a} {b=c} set_a set_c (transitive @{w} ltab ltbc)
    transitive (Direct {b=b} set_a set_b1 ltab) (Indirect {a=b} set_b1 set_b2 lt12) = Direct set_a set_b2 ltab
    transitive (Indirect {a=a} set_a1 set_a2 lt12) (Direct _ set_b ltab) = Direct set_a1 set_b ltab
    transitive (Indirect {a=a} @{wg1} set1 set2 lt12) (Indirect {a=a} @{wg2} set2 set3 lt23) with (decEqWOrdImplem wg1 wg2)
      transitive (Indirect {a=a} @{wg1} set1 set2 lt12) (Indirect {a=a} @{wg2} set2 set3 lt23) | No _ = believe_me ""
      transitive (Indirect {a=a} @{wg} set1 set2 lt12) (Indirect {a=a} @{wg} set2 set3 lt23) | Yes Refl = Indirect set1 set3 (transitive @{wg} lt12 lt23)



    strict NilBase impossible
    strict (Direct {a=a} @{w} _ _ ltaa) = strict @{w} ltaa
    strict (Indirect {w} @{wg} _ _ lt11) = strict @{wg} lt11



    total_order [] [] = Grn Refl
    total_order [] (Some x) = Red NilBase
    total_order (Some x) [] = Blu NilBase
    total_order (Some (Single a)) (Some (Single b)) with (total_order a b)
      total_order (Some (Single a)) (Some (Single b)) | Red ltab = Red $ Direct (Single a) (Single b) ltab
      total_order (Some (Single a)) (Some (Single a)) | Grn Refl = Grn Refl
      total_order (Some (Single a)) (Some (Single b)) | Blu ltba = Blu $ Direct (Single b) (Single a) ltba
    total_order (Some (Single a)) (Some (Cons b x z)) with (total_order a b)
      total_order (Some (Single a)) (Some (Cons b x z)) | Red ltab = Red $ Direct (Single a) (Cons b x z) ltab
      total_order (Some (Single a)) (Some (Cons a ltab set_b)) | Grn Refl = Red $ Indirect @{wordgneset} (Single a) (Cons a ltab set_b) (GNELTSingle ltab set_b)
      total_order (Some (Single a)) (Some (Cons b x z)) | Blu ltba = Blu $ Direct (Cons b x z) (Single a) ltba
    total_order (Some (Cons a z s)) (Some (Single b)) with (total_order a b)
      total_order (Some (Cons a z s)) (Some (Single b)) | Red ltab = Red $ Direct (Cons a z s) (Single b) ltab
      total_order (Some (Cons a ltab set_b)) (Some (Single a)) | Grn Refl = Blu $ Indirect @{wordgneset} (Single a) (Cons a ltab set_b) (GNELTSingle ltab set_b)
      total_order (Some (Cons a z s)) (Some (Single b)) | Blu ltba = Blu $ Direct (Single b) (Cons a z s) ltba
    total_order (Some (Cons a ltab set_b)) (Some (Cons c ltcd set_d)) with (total_order a c)
      total_order (Some (Cons a ltab set_b)) (Some (Cons c ltcd set_d)) | Red ltac = Red $ Direct (Cons a ltab set_b) (Cons c ltcd set_d) ltac
      total_order (Some (Cons a ltab set_b)) (Some (Cons c ltcd set_d)) | Blu ltca = Blu $ Direct (Cons c ltcd set_d) (Cons a ltab set_b) ltca
      total_order (Some (Cons a ltab set_b)) (Some (Cons a ltad set_d)) | Grn Refl with (total_order @{wordgneset} (Cons a ltab set_b) (Cons a ltad set_d))
        total_order (Some (Cons a ltab set_b)) (Some (Cons a ltad set_d)) | Grn Refl | Red lt12 = Red $ Indirect @{wordgneset} (Cons a ltab set_b) (Cons a ltad set_d) lt12
        total_order (Some (Cons a ltab set_b)) (Some (Cons a ltab set_b)) | Grn Refl | Grn Refl = Grn Refl
        total_order (Some (Cons a ltab set_b)) (Some (Cons a ltad set_d)) | Grn Refl | Blu lt21 = Blu $ Indirect @{wordgneset} (Cons a ltad set_d) (Cons a ltab set_b) lt21



    well_founded [] NilBase impossible
    well_founded [] (Direct _ _ _) impossible
    well_founded [] (Indirect _ _ _) impossible
    well_founded (Some x) NilBase impossible
    well_founded (Some x) (Direct _ _ _) impossible
    well_founded (Some x) (Indirect _ _ _) impossible



    mere_prop NilBase NilBase = Refl
    mere_prop (Direct {a=a} {b=b} @{w} set_a set_b ltab) (Direct {a=a} {b=b} @{w} set_a set_b ltab') = rewrite (mere_prop @{w} {a=a} {b=b} ltab ltab') in Refl
    mere_prop (Direct {a=a} @{w} set1 set2 ltaa) (Indirect set1 set2 _) = void $ strict @{w} {a=a} ltaa
    mere_prop (Indirect set1 set2 x) (Direct {a=a} @{w} set1 set2 ltaa) = void $ strict @{w} {a=a} ltaa
    mere_prop (Indirect {w} @{wg1} set1 set2 x) (Indirect {w} @{wg2} set1 set2 y) with (decEqWOrdImplem wg1 wg2)
      mere_prop (Indirect {w} @{wg1} set1 set2 x) (Indirect {w} @{wg2} set1 set2 y) | No _ = believe_me ""
      mere_prop (Indirect {w} @{wg} set1 set2 x) (Indirect {w} @{wg} set1 set2 y) | Yes Refl = rewrite (mere_prop @{wg} {a=set1} {b=set2} y x) in Refl



