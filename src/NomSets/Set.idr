module Set

import NomSets.Atom
import NomSets.SLT
import NomSets.TotalOrdering
import NomSets.NESet

data Set : Type where
  Nil  : Set
  Some : NESet n -> Set

add : Atom -> Set -> Set
add n Nil = Some (Single n)
add n (Some s) = Some (add n s)

merge : Set -> Set -> Set
merge Nil x = x
merge (Some x) Nil = Some x
merge (Some x) (Some y) = Some (merge x y)

Semigroup Set where
  (<+>) = merge
Monoid Set where
  neutral = Nil

Uninhabited (Nil = Some x) where
  uninhabited Refl impossible
Uninhabited (Some x = Nil) where
  uninhabited Refl impossible

eq_some_destruct : (x : NESet a) -> (y : NESet b) -> (Some x = Some y) -> x = y
eq_some_destruct _ _ Refl = Refl

DecEq Set where
  decEq Nil Nil = Yes Refl
  decEq Nil (Some _) = No absurd
  decEq (Some _) Nil = No absurd
  decEq (Some x) (Some y) with (nesetDecEqH x y)
    decEq (Some x) (Some x) | Yes Refl = Yes Refl
    decEq (Some x) (Some y) | No contra = No (contra . (eq_some_destruct x y))

Eq Set where
    x == y with (decEq x y)
      x == x | Yes Refl = True
      x == y | No _ = False

Ord Set where
  compare Nil Nil = EQ
  compare Nil (Some x) = LT
  compare (Some x) Nil = GT
  compare (Some x) (Some y) = nesetCompareH x y
