module NomSets.TotalOrdering

import NomSets.SLT

%access public export
%default total

infix 4 <=?>

data (<=?>) : Nat -> Nat -> Type where
  TLT : m !<! n -> m <=?> n
  TEQ : m  =  n -> m <=?> n
  TGT : n !<! m -> m <=?> n

TotalOrdering : Nat -> Nat -> Type
TotalOrdering = (<=?>)

tord : (a:Nat) -> (b:Nat) -> a <=?> b
tord Z Z = TEQ Refl
tord Z (S k) = TLT SLTZ
tord (S j) Z = TGT SLTZ
tord (S j) (S k) = case tord j k of
                       TLT p => TLT (SLTS p)
                       TEQ p => TEQ (cong $ p)
                       TGT p => TGT (SLTS p)

tord_reverse : m <=?> n -> n <=?> m
tord_reverse (TLT p) = TGT p
tord_reverse (TEQ prf) = TEQ $ sym prf
tord_reverse (TGT p) = TLT p
