module NomSets.GNEElem

import public NomSets.GNESet

%access public export
%default total


-- Data type used for "decContains/contains" predicate
data GNEElem : {t:Type} -> {a:t} -> t -> GNESet t a -> Type where
  GNEHere  : {set_a : GNESet t a}
           -> GNEElem a set_a
  GNEThere : WOrd t => {a,b : t}
           -> GNEElem a set_c
           -> GNEElem a (Cons b ltbc set_c)


--==== START LEMMAS

a_elem_singleb_implies_not_ltba : WOrd t => {a,b : t}
                                -> GNEElem a (Single b)
                                -> Not (b !<<! a)
a_elem_singleb_implies_not_ltba {a} GNEHere = \ltaa => void_strict a ltaa



ltba_implies_not_a_elem_singleb : WOrd t => {a,b : t}
                                -> b !<<! a
                                -> Not (GNEElem a (Single b))
ltba_implies_not_a_elem_singleb {a} ltaa GNEHere = void_strict a ltaa



a_notin_setc_implies_a_notin_cons_b_setc : WOrd t => {a,b : t}
                                         -> b !<<! a
                                         -> Not (GNEElem a set_c)
                                         -> Not (GNEElem a (Cons b ltbc set_c))
a_notin_setc_implies_a_notin_cons_b_setc {a} ltaa contra GNEHere = void_strict a ltaa
a_notin_setc_implies_a_notin_cons_b_setc ltba contra (GNEThere rest) = contra rest



ltab_implies_not_a_elem : (w:WOrd t) => {a,b:t}
                        -> (set : GNESet t b)
                        -> a !<<! b
                        -> Not (GNEElem a set)
ltab_implies_not_a_elem {a} _ ltaa GNEHere = void_strict a ltaa
ltab_implies_not_a_elem @{w} {a} {b} (Cons {y=c} @{w1} b ltbc set_c) ltab (GNEThere rest) with (decEqWOrdImplem w w1)
  ltab_implies_not_a_elem @{w} {a} {b} (Cons {y=c} @{w1} b ltbc set_c) ltab (GNEThere rest) | No contra = believe_me "It can't happen"
  ltab_implies_not_a_elem @{w} {a} {b} (Cons {y=c} @{w} b ltbc set_c)  ltab (GNEThere rest) | Yes Refl = ltab_implies_not_a_elem @{w} set_c (transitive {a=a} {b=b} ltab ltbc) rest


--==== END LEMMAS


decContains : WOrd t => (a:t)
            -> (set_b : GNESet t b)
            -> Dec (GNEElem a set_b)
decContains @{w} a (Single b) with (total_order a b)
  decContains @{w} a (Single b) | Red ltab = No $ ltab_implies_not_a_elem (Single b) ltab
  decContains @{w} a (Single a) | Grn Refl = Yes GNEHere
  decContains @{w} a (Single b) | Blu ltba = No $ ltba_implies_not_a_elem_singleb ltba
decContains @{w} a (Cons b ltbc set_c) with (total_order a b)
  decContains @{w} a (Cons b ltbc set_c) | Red ltab = No $ ltab_implies_not_a_elem (Cons b ltbc set_c) ltab
  decContains @{w} a (Cons a ltbc set_c) | Grn Refl = Yes GNEHere
  decContains @{w} a (Cons b ltbc set_c) | Blu ltba with (decContains a set_c)
    decContains @{w} a (Cons b ltbc set_c) | Blu ltba | Yes prf   = Yes $ GNEThere prf
    decContains @{w} a (Cons b ltbc set_c) | Blu ltba | No contra = No $ a_notin_setc_implies_a_notin_cons_b_setc ltba contra


contains : WOrd t => (a:t)
         -> GNESet t b
         -> Bool
contains a set_b with (decContains a set_b)
  contains a set_b | Yes _ = True
  contains a set_b | No  _ = False


