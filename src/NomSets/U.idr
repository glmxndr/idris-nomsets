module NomSets.U

import public NomSets.Atom
import public NomSets.GSet

%access public export
%default total

data U : Nat -> Type where
  U0 : Atom -> U 0
  U1 : GSet Atom -> U 1
  Un : (U n, GSet (U n)) -> U (S n)

rank : U n -> Nat
rank (U0 _) = 0
rank (U1 _) = 1
rank (Un (x, Nil)) = rank x
rank {n=S n} (Un (x, Some y)) = S n

infix 4 :<
data AtomPair : Type where
  (:<) : (a:Atom) -> (b:Atom) -> {auto ltab: a !<! b} -> AtomPair

permutU1 : AtomPair -> GNESet Atom x -> (y ** GNESet Atom y)
permutU1     (a:<b) (Single c) with (decEq a c)
  permutU1   (a:<b) (Single a) | Yes Refl = (b ** Single b)
  permutU1   (a:<b) (Single c) | No _ with (decEq b c)
    permutU1 (a:<b) (Single b) | No _ | Yes Refl = (a ** Single a)
    permutU1 (a:<b) (Single c) | No _ | No _     = (c ** Single c)
permutU1       (a:<b) (Cons {y=d} c ltcd set_d) with (total_order a c)
  permutU1     (a:<b) (Cons {y=d} c ltcd set_d) | Red ltac = (c ** (Cons c ltcd set_d))
  permutU1     (a:<b) (Cons {y=d} a ltad set_d) | Grn Refl = (a ** (Cons a ?ltaminbd (add b set_d)))
  permutU1     (a:<b) (Cons {y=d} c ltcd set_d) | Blu ltca = (c ** (Cons c ?ltcminad (permutU1 a set_d)))


permut : (Atom, Atom) -> U n -> U n
-- permut (a, b) (U0 a) = U0 b
-- permut (a, b) (U0 b) = U0 a
-- permut (a, b) u = u
-- permut (a, b) (U1 Nil) = U1 Nil
-- permut (a, b) (U1 (Some s)) = U1 (Some (permut (a,b) s))
