module NomSets.WOrd.Lemmas

import NomSets.WOrd

%access public export
%default total

--====================

wmin_a_a_is_a : WOrd t => (a:t)
              -> wmin a a = a
wmin_a_a_is_a a with (total_order a a)
  wmin_a_a_is_a a | Red ltaa = void_strict a ltaa
  wmin_a_a_is_a a | Grn Refl = Refl
  wmin_a_a_is_a a | Blu ltaa = void_strict a ltaa

--====================

wmin_symm : WOrd t => (a:t) -> (b:t)
          -> wmin a b = wmin b a
wmin_symm a b with (total_order a b)
  wmin_symm a b   | Red ltab with (total_order b a)
    wmin_symm a b | Red ltab | Red ltba = void_as a b ltab ltba
    wmin_symm a a | Red ltaa | Grn Refl = void_strict a ltaa
    wmin_symm a b | Red ltab | Blu ltba = Refl
  wmin_symm a a   | Grn Refl with (total_order a a)
    wmin_symm a a | Grn Refl | Red ltaa = void_strict a ltaa
    wmin_symm a a | Grn Refl | Grn Refl = Refl
    wmin_symm a a | Grn Refl | Blu ltaa = void_strict a ltaa
  wmin_symm a b   | Blu ltba with (total_order b a)
    wmin_symm a b | Blu ltba | Red ltab = Refl
    wmin_symm a a | Blu ltaa | Grn Refl = void_strict a ltaa
    wmin_symm a b | Blu ltba | Blu ltab = void_as a b ltab ltba

--====================

wmin_assoc : WOrd t => (a:t) -> (b:t) -> (c:t)
           -> wmin a (wmin b c) = wmin (wmin a b) c
wmin_assoc a b c with (total_order b c)
  wmin_assoc a b c | Red ltbc with (total_order a b)
    wmin_assoc a b c | Red ltbc | Red ltab with (total_order a c)
      wmin_assoc a b c | Red ltbc | Red ltab | Red ltac = Refl {x=a}
      wmin_assoc a b a | Red ltba | Red ltab | Grn Refl = void_as a b ltab ltba
      wmin_assoc a b c | Red ltbc | Red ltab | Blu ltca = void_acyclic a b c ltab ltbc ltca
    wmin_assoc a a c | Red ltac | Grn Refl with (total_order a c)
      wmin_assoc a a c | Red ltac | Grn Refl | Red _    = Refl {x=a}
      wmin_assoc a a a | Red ltaa | Grn Refl | Grn Refl = void_strict a ltaa
      wmin_assoc a a c | Red ltac | Grn Refl | Blu ltca = void_as a c ltac ltca
    wmin_assoc a b c | Red ltbc | Blu ltba with (total_order b c)
      wmin_assoc a b c | Red ltbc | Blu ltba | Red _    = Refl {x=b}
      wmin_assoc a b b | Red ltbb | Blu ltba | Grn Refl = void_strict b ltbb
      wmin_assoc a b c | Red ltbc | Blu ltba | Blu ltcb = void_as b c ltbc ltcb
  wmin_assoc a b b | Grn Refl with (total_order a b)
    wmin_assoc a b b | Grn Refl | Red ltab with (total_order a b)
      wmin_assoc a b b | Grn Refl | Red ltab | Red _    = Refl {x=a}
      wmin_assoc a a a | Grn Refl | Red ltaa | Grn Refl = void_strict a ltaa
      wmin_assoc a b b | Grn Refl | Red ltab | Blu ltba = void_as a b ltab ltba
    wmin_assoc a a a | Grn Refl | Grn Refl with (total_order a a)
      wmin_assoc a a a | Grn Refl | Grn Refl | Red ltaa = void_strict a ltaa
      wmin_assoc a a a | Grn Refl | Grn Refl | Grn Refl = Refl {x=a}
      wmin_assoc a a a | Grn Refl | Grn Refl | Blu ltaa = void_strict a ltaa
    wmin_assoc a b b | Grn Refl | Blu ltba with (total_order b b)
      wmin_assoc a b b | Grn Refl | Blu ltba | Red ltbb = void_strict b ltbb
      wmin_assoc a b b | Grn Refl | Blu ltba | Grn Refl = Refl {x=b}
      wmin_assoc a b b | Grn Refl | Blu ltba | Blu ltbb = void_strict b ltbb
  wmin_assoc a b c | Blu ltcb with (total_order a b)
    wmin_assoc a b c | Blu ltcb | Red ltab with (total_order a c)
      wmin_assoc a b c | Blu ltcb | Red ltab | Red ltac = Refl {x=a}
      wmin_assoc a b a | Blu ltab | Red _    | Grn Refl = Refl {x=a}
      wmin_assoc a b c | Blu ltcb | Red ltab | Blu ltca = Refl {x=c}
    wmin_assoc a a c | Blu ltcb | Grn Refl with (total_order a c)
      wmin_assoc a a c | Blu ltca | Grn Refl | Red ltac = Refl {x=a}
      wmin_assoc a a a | Blu ltaa | Grn Refl | Grn Refl = void_strict a ltaa
      wmin_assoc a a c | Blu ltca | Grn Refl | Blu _    = Refl {x=c}
    wmin_assoc a b c | Blu ltcb | Blu ltba with (total_order a c)
      wmin_assoc a b c | Blu ltcb | Blu ltba | Red ltac = void_acyclic a c b ltac ltcb ltba
      wmin_assoc a b a | Blu ltab | Blu ltba | Grn Refl = void_as a b ltab ltba
      wmin_assoc a b c | Blu ltcb | Blu ltba | Blu ltca with (total_order b c)
        wmin_assoc a b c | Blu ltcb | Blu ltba | Blu ltca | Red ltbc = void_as b c ltbc ltcb
        wmin_assoc a b b | Blu ltbb | Blu ltba | Blu _    | Grn Refl = void_strict b ltbb
        wmin_assoc a b c | Blu ltcb | Blu ltba | Blu ltca | Blu _    = Refl {x=c}

--====================

ltab_implies_minab_is_a : WOrd t => (a:t) -> (b:t)
                        -> a !<<! b
                        -> wmin a b = a
ltab_implies_minab_is_a a b ltab with (total_order a b)
  ltab_implies_minab_is_a a b ltab | Red _     = Refl
  ltab_implies_minab_is_a a a ltaa | Grn Refl  = void_strict a ltaa
  ltab_implies_minab_is_a a b ltab | Blu ltba  = void_as a b ltab ltba

--====================

ltab_implies_minba_is_a : WOrd t => (a:t) -> (b:t)
                        -> a !<<! b
                        -> wmin b a = a
ltab_implies_minba_is_a a b ltab = rewrite wmin_symm b a in ltab_implies_minab_is_a a b ltab

--====================

ltab_ltac_implies_ltaminbc : WOrd t => (a:t) -> (b:t) -> (c:t)
                           -> a !<<! b
                           -> a !<<! c
                           -> a !<<! (wmin b c)
ltab_ltac_implies_ltaminbc a b c ltab ltac with (total_order b c)
  ltab_ltac_implies_ltaminbc a b c ltab ltac | Red ltbc = ltab
  ltab_ltac_implies_ltaminbc a b b ltab _    | Grn Refl = ltab
  ltab_ltac_implies_ltaminbc a b c ltab ltac | Blu ltcb = ltac

--====================

ltab_ltac_implies_minab_is_minac : WOrd t => (a:t) -> (b:t) -> (c:t)
                                 -> a !<<! b
                                 -> a !<<! c
                                 -> wmin a b = wmin a c
ltab_ltac_implies_minab_is_minac a b c ltab ltac =
  rewrite ltab_implies_minab_is_a a b ltab in
  rewrite ltab_implies_minab_is_a a c ltac in Refl

--====================

ltab_implies_minaa_is_minab : WOrd t => (a:t) -> (b:t)
                             -> a !<<! b
                             -> wmin a a = wmin a b
ltab_implies_minaa_is_minab a b ltab =
  rewrite ltab_implies_minab_is_a a b ltab in
  rewrite wmin_a_a_is_a a in Refl

--====================

ltab_ltac_implies_minac_is_minaminbc : WOrd t => (a:t) -> (b:t) -> (c:t)
                                     -> a !<<! b
                                     -> a !<<! c
                                     -> wmin a c = wmin a (wmin b c)
ltab_ltac_implies_minac_is_minaminbc a b c ltab ltac =
  let lta_minbc = ltab_ltac_implies_ltaminbc a b c ltab ltac in
  ltab_ltac_implies_minab_is_minac a c (wmin b c) ltac lta_minbc

--====================

ltab_ltac_implies_minba_is_minaminbc : WOrd t => (a:t) -> (b:t) -> (c:t)
                                     -> a !<<! b
                                     -> a !<<! c
                                     -> wmin b a = wmin a (wmin b c)
ltab_ltac_implies_minba_is_minaminbc a b c ltab ltac =
  let lta_minbc = ltab_ltac_implies_ltaminbc a b c ltab ltac in
  rewrite wmin_symm b a in
  ltab_ltac_implies_minab_is_minac a b (wmin b c) ltab lta_minbc

--====================

ltab_ltac_implies_minab_is_minaminbc : WOrd t => (a:t) -> (b:t) -> (c:t)
                                     -> a !<<! b
                                     -> a !<<! c
                                     -> wmin a b = wmin a (wmin b c)
ltab_ltac_implies_minab_is_minaminbc a b c ltab ltac =
  let lta_minbc = ltab_ltac_implies_ltaminbc a b c ltab ltac in
  ltab_ltac_implies_minab_is_minac a b (wmin b c) ltab lta_minbc

--====================

ltab_ltac_implies_minaa_is_minaminbc : WOrd t => (a:t) -> (b:t) -> (c:t)
                                     -> a !<<! b -> a !<<! c
                                     -> wmin a a = wmin a (wmin b c)
ltab_ltac_implies_minaa_is_minaminbc a b c ltab ltac =
  rewrite ltab_implies_minaa_is_minab a b ltab in
  rewrite wmin_symm a b in
  ltab_ltac_implies_minba_is_minaminbc a b c ltab ltac

--====================

ltab_ltac_implies_a_is_minaminbc : WOrd t => (a:t) -> (b:t) -> (c:t)
                                     -> a !<<! b
                                     -> a !<<! c
                                     -> a = wmin a (wmin b c)
ltab_ltac_implies_a_is_minaminbc a b c ltab ltac with (total_order b c)
  ltab_ltac_implies_a_is_minaminbc a b c ltab ltac | Red ltbc with (total_order a b)
    ltab_ltac_implies_a_is_minaminbc a b c ltab ltac | Red ltbc | Red _     = Refl
    ltab_ltac_implies_a_is_minaminbc a a c ltaa ltac | Red _    | Grn Refl  = void_strict a ltaa
    ltab_ltac_implies_a_is_minaminbc a b c ltab ltac | Red ltbc | Blu ltba  = void_as a b ltab ltba
  ltab_ltac_implies_a_is_minaminbc a b b ltab _    | Grn Refl with (total_order a b)
    ltab_ltac_implies_a_is_minaminbc a b b ltab _    | Grn Refl | Red _     = Refl
    ltab_ltac_implies_a_is_minaminbc a a a ltaa _    | Grn Refl | Grn Refl  = void_strict a ltaa
    ltab_ltac_implies_a_is_minaminbc a b b ltab _    | Grn Refl | Blu ltba  = void_as a b ltab ltba
  ltab_ltac_implies_a_is_minaminbc a b c ltab ltac | Blu ltcb with (total_order a c)
    ltab_ltac_implies_a_is_minaminbc a b c ltab ltac | Blu ltcb | Red _     = Refl
    ltab_ltac_implies_a_is_minaminbc a b a ltab ltaa | Blu ltcb | Grn Refl  = void_strict a ltaa
    ltab_ltac_implies_a_is_minaminbc a b c ltab ltac | Blu ltcb | Blu ltca  = void_as a c ltac ltca

--====================
