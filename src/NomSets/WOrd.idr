module NomSets.WOrd

import public NomSets.Atom
import public NomSets.RGB
import public NomSets.SLT

%access public export
%default total

infix 4 !<<!

interface DecEq t => WOrd t | t where
  constructor MkWOrd
  (!<<!) : t -> t -> Type
  antisymmetric : a !<<! b -> Not (b !<<! a)
  transitive : a !<<! b -> b !<<! c -> a !<<! c
  strict : Not (a !<<! a)
  total_order : (a:t) -> (b:t) -> RGB (a !<<! b) (a = b) (b !<<! a)
  base : t
  well_founded : (x:t) -> Not (x !<<! base)
  mere_prop : (l : a !<<! b) -> (m : a !<<! b) -> l = m


--=================

strict_eq : WOrd t => {a:t} -> {b:t}
          -> a !<<! b -> Not (a = b)
strict_eq {a} ltaa Refl = strict {a=a} ltaa

--=================

acyclic : WOrd t => {a:t} -> {b:t} -> {c:t}
        -> a !<<! b
        -> b !<<! c
        -> Not (c !<<! a)
acyclic {a} {b} {c} ltab ltbc ltca =
  let ltac = transitive {a=a} {b=b} {c=c} ltab ltbc in
  antisymmetric {a=a} {b=c} ltac ltca

--=================

neqType_neqValue : {x:Type} -> {y:Type} -> {a:x} -> {b:y}
                 -> Not (x=y)
                 -> Not (a=b)
neqType_neqValue {x} {a} contra Refl = contra Refl



decEqH : WOrd t => {a:t} -> {b:t} -> {c:t} -> {d:t}
       -> (l : a !<<! b)
       -> (m : c !<<! d)
       -> Dec (l = m)
decEqH {a} {b} {c} {d}   l m with (decEq a c)
  decEqH {a} {b} {d}     l m | Yes Refl with (decEq b d)
    decEqH {a} {b}       l m | Yes Refl | Yes Refl = Yes $ mere_prop {a=a} {b=b} l m
    decEqH {a} {b} {d}   l m | Yes Refl | No contra = No $ neqHighend_neqType contra
    where
      highend_eq : l = m -> b = d
      highend_eq prf = believe_me prf
      neqHighend_neqType : Not (b = d) -> Not (l = m)
      neqHighend_neqType contra prf = contra $ highend_eq prf
  decEqH {a} {b} {c} {d} l m | No contra = No $ neqLowend_neqType contra
  where
    lowend_eq : l = m -> a = c
    lowend_eq prf = believe_me prf
    neqLowend_neqType : Not (a = c) -> Not (l = m)
    neqLowend_neqType contra prf = contra $ lowend_eq prf

--=================

void_strict : WOrd t => (a:t) -> a !<<! a -> r
void_strict a ltaa = void $ strict {a=a} ltaa



void_as : WOrd t => (a:t) -> (b:t) -> a !<<! b -> b !<<! a -> r
void_as a b ltab ltba = void $ antisymmetric {a=a} {b=b} ltab ltba



void_acyclic : WOrd t => (a:t) -> (b:t) -> (c:t) -> a !<<! b -> b !<<! c -> c !<<! a -> r
void_acyclic a b c ltab ltbc ltca = void $ acyclic {a=a} {b=b} {c=c} ltab ltbc ltca

--=================

wmin : WOrd t => t -> t -> t
wmin a b with (total_order a b)
  wmin a b | Red ltab = a
  wmin a a | Grn Refl = a
  wmin a b | Blu ltba = b

--=================

namespace WOrdNat

  won_S_destruct : S a !<! S b -> a !<! b
  won_S_destruct (SLTS p) = p

  won_as : {a:Nat} -> {b:Nat} -> a !<! b -> Not (b !<! a)
  won_as {a = Z} {b = Z} ltab ltba impossible
  won_as {a = Z} {b = (S k)} ltab ltba impossible
  won_as {a = (S k)} {b = Z} ltab ltba impossible
  won_as {a = (S k)} {b = (S j)} ltab ltba =
    won_as (won_S_destruct ltab) (won_S_destruct ltba)
  won_t : {a:Nat} -> {b:Nat} -> {c:Nat}
        -> a !<! b -> b !<! c -> a !<! c
  won_t {a = Z} {b = Z} {c = c} ltab ltbc impossible
  won_t {a = Z} {b = (S k)} {c = Z} ltab ltbc impossible
  won_t {a = Z} {b = (S k)} {c = (S j)} SLTZ (SLTS p) = SLTZ
  won_t {a = (S k)} {b = Z} {c = c} ltab ltbc impossible
  won_t {a = (S k)} {b = (S i)} {c = Z} ltab ltbc impossible
  won_t {a = (S k)} {b = (S i)} {c = (S j)} (SLTS p) (SLTS q) = SLTS $ won_t p q

  won_to : (a:Nat) -> (b:Nat)
         -> RGB (a !<! b) (a = b) (b !<! a)
  won_to Z Z = Grn Refl
  won_to Z (S k) = Red SLTZ
  won_to (S k) Z = Blu SLTZ
  won_to (S k) (S j) with (won_to {a=k} {b=j})
    won_to (S k) (S j) | Red ltkj = Red (SLTS ltkj)
    won_to (S j) (S j) | Grn Refl = Grn Refl
    won_to (S k) (S j) | Blu ltjk = Blu (SLTS ltjk)

  Uninhabited (0 !<! 0) where
    uninhabited SLTZ impossible
    uninhabited (SLTS _) impossible
  Uninhabited (S n !<! 0) where
    uninhabited SLTZ impossible
    uninhabited (SLTS _) impossible
  Uninhabited (S n !<! S n) where
    uninhabited SLTZ impossible
    uninhabited (SLTS SLTZ) impossible
    uninhabited (SLTS s@(SLTS r)) = uninhabited s

  won_wf : (n:Nat) -> Not (n !<! 0)
  won_wf Z = absurd
  won_wf (S n) = absurd

  won_strict : {a:Nat} -> Not (a !<! a)
  won_strict {a=Z} = absurd
  won_strict {a=S n} = absurd

  won_mprop : (l : a !<! b) -> (m : a !<! b) -> l = m
  won_mprop SLTZ SLTZ = Refl
  won_mprop (SLTS z) (SLTS x) = cong $ won_mprop z x

WOrd Nat where
  (!<<!) = SLT
  antisymmetric = won_as
  transitive = won_t
  total_order = won_to
  strict = won_strict
  base = Z
  well_founded = won_wf
  mere_prop = won_mprop

