module NomSets.SLT

%access public export
%default total

-- Strictly Lower Than
infix 4 !<!
data (!<!) : Nat -> Nat -> Type where
  SLTZ : Z !<! S n
  SLTS : x !<! y -> S x !<! S y

SLT : Nat -> Nat -> Type
SLT = (!<!)

slt_low : a !<! b -> Nat
slt_low {a} _ = a
slt_hgh : a !<! b -> Nat
slt_hgh {a} {b} _ = b

lt01 : 0 !<! 1
lt01 = SLTZ
lt12 : 1 !<! 2
lt12 = SLTS SLTZ
lt02 : 0 !<! 2
lt02 = SLTZ

%hint
ltes_to_slt : LT m n -> m !<! n
ltes_to_slt LTEZero impossible
ltes_to_slt (LTESucc LTEZero) = SLTZ
ltes_to_slt (LTESucc p@(LTESucc _)) = SLTS $ ltes_to_slt p

%hint
ltab_ltac_eqbc_implies_eqltabltac : (ltab : a !<! b)
                                  -> (ltac : a !<! c)
                                  -> b = c
                                  -> ltab = ltac
ltab_ltac_eqbc_implies_eqltabltac SLTZ SLTZ Refl = Refl
ltab_ltac_eqbc_implies_eqltabltac SLTZ (SLTS _) Refl impossible
ltab_ltac_eqbc_implies_eqltabltac (SLTS _) SLTZ Refl impossible
ltab_ltac_eqbc_implies_eqltabltac (SLTS x) (SLTS y) Refl = cong $ ltab_ltac_eqbc_implies_eqltabltac x y Refl

slt_pi : (p1 : a !<! b) -> (p2 : a !<! b) -> p1 = p2
slt_pi SLTZ SLTZ = Refl
slt_pi (SLTS x) (SLTS y) = cong $ slt_pi x y

slt_diffa_noteq : (ltab : a !<! b) -> (ltcd : c !<! d) -> Not (a = c) -> Not (ltab = ltcd)
slt_diffa_noteq SLTZ SLTZ neac Refl = neac Refl
slt_diffa_noteq SLTZ (SLTS y) neac Refl impossible
slt_diffa_noteq (SLTS z) SLTZ neac Refl impossible
slt_diffa_noteq (SLTS y) (SLTS y) neac Refl = neac Refl

slt_diffb_noteq : (ltab : a !<! b) -> (ltcd : c !<! d) -> Not (b = d) -> Not (ltab = ltcd)
slt_diffb_noteq SLTZ SLTZ nebd Refl = nebd Refl
slt_diffb_noteq SLTZ (SLTS y) nebd Refl impossible
slt_diffb_noteq (SLTS z) SLTZ nebd Refl impossible
slt_diffb_noteq (SLTS y) (SLTS y) nebd Refl = nebd Refl

sltDecEqH : (ltab : a !<! b) -> (ltcd : c !<! d) -> Dec (ltab = ltcd)
sltDecEqH {a} {b} {c} {d} ltab ltcd with (decEq a c)
  sltDecEqH {a} {b} {d} ltab ltcd | Yes Refl with (decEq b d)
    sltDecEqH {a} {b} ltab ltac | Yes Refl | Yes Refl = Yes $ slt_pi ltab ltac
    sltDecEqH {a} {b} ltab ltcd | Yes Refl | No contra = No (slt_diffb_noteq ltab ltcd contra)
  sltDecEqH {a} {b} {c} {d} ltab ltcd | No contra = No (slt_diffa_noteq ltab ltcd contra)

DecEq (a !<! b) where
  decEq = sltDecEqH

Eq (a !<! b) where
  ltab1 == ltab2 with (decEq ltab1 ltab2)
    ltab1 == ltab2 | Yes _ = True
    ltab1 == ltab2 | No  _ = False

