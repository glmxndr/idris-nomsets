module NomSets.GNELT

import public NomSets.GNESet

%access public export
%default total



data GNELT : GNESet t a -> GNESet t a -> Type where
  GNELTSingle : WOrd t => {a,b : t}
                       -> (ltab : a !<<! b)
                       -> (set_b : GNESet t b)
                       -> GNELT (Single a) (Cons a ltab set_b)
  GNELTInd    : (w:WOrd t) => {a, b : t}
                           -> (ltab : a !<<! b)
                           -> (set1 : GNESet t b)
                           -> (set2 : GNESet t b)
                           -> GNELT set1 set2
                           -> GNELT (Cons @{w} a ltab set1) (Cons @{w} a ltab set2)
  GNELTRest   : (w:WOrd t) => {a,b,c : t}
                           -> (ltab : a !<<! b)
                           -> (ltac : a !<<! c)
                           -> (ltbc : b !<<! c)
                           -> (set_b : GNESet t b)
                           -> (set_c : GNESet t c)
                           -> GNELT (Cons @{w} a ltab set_b) (Cons @{w} a ltac set_c)


namespace WOrdGNELT

  wog_base : WOrd t => {a:t} -> GNESet t a
  wog_base {a} = Single a



  wog_to : (w:WOrd t) => {a : t}
                      -> (s1 : GNESet t a)
                      -> (s2 : GNESet t a)
                      -> RGB (GNELT s1 s2) (s1 = s2) (GNELT s2 s1)
  wog_to @{w} (Single a) (Single a) = Grn Refl
  wog_to @{w} (Single a) (Cons a x z) = Red (GNELTSingle _ _)
  wog_to @{w} (Cons a x z) (Single a) = Blu (GNELTSingle _ _)
  wog_to       @{w} (Cons {y=b} @{x} a ltab set_b) (Cons {y=c} @{y} a ltac set_c) with (decEqWOrdImplem w x)
   wog_to      @{w} (Cons {y=b} @{x} a ltab set_b) (Cons {y=c} @{y} a ltac set_c) | No _ = believe_me ""
   wog_to      @{w} (Cons {y=b} @{w} a ltab set_b) (Cons {y=c} @{y} a ltac set_c) | Yes Refl with (decEqWOrdImplem w y)
    wog_to     @{w} (Cons {y=b} @{w} a ltab set_b) (Cons {y=c} @{y} a ltac set_c) | Yes Refl | No _ = believe_me ""
    wog_to     @{w} (Cons {y=b} @{w} a ltab set_b) (Cons {y=c} @{w} a ltac set_c) | Yes Refl | Yes Refl with (total_order b c)
     wog_to    @{w} (Cons {y=b} @{w} a ltab set_b) (Cons {y=c} @{w} a ltac set_c) | Yes Refl | Yes Refl | Red ltbc = Red (GNELTRest ltab ltac ltbc set_b set_c)
     wog_to    @{w} (Cons {y=b} @{w} a ltab set_b) (Cons {y=c} @{w} a ltac set_c) | Yes Refl | Yes Refl | Blu ltcb = Blu (GNELTRest ltac ltab ltcb set_c set_b)
     wog_to    @{w} (Cons {y=b} @{w} a ltab set_b) (Cons {y=b} @{w} a ltab' set_b') | Yes Refl | Yes Refl | Grn Refl with (wog_to @{w} set_b set_b')
      wog_to   @{w} (Cons {y=b} @{w} a ltab set_b) (Cons {y=b} @{w} a ltab' set_b') | Yes Refl | Yes Refl | Grn Refl | Red ltbb' = rewrite (mere_prop @{w} ltab' ltab) in Red (GNELTInd ltab set_b set_b' ltbb')
      wog_to   @{w} (Cons {y=b} @{w} a ltab set_b) (Cons {y=b} @{w} a ltab' set_b)  | Yes Refl | Yes Refl | Grn Refl | Grn Refl = rewrite (mere_prop @{w} ltab' ltab) in Grn Refl
      wog_to   @{w} (Cons {y=b} @{w} a ltab set_b) (Cons {y=b} @{w} a ltab' set_b') | Yes Refl | Yes Refl | Grn Refl | Blu ltb'b = rewrite (mere_prop @{w} ltab' ltab) in Blu (GNELTInd ltab set_b' set_b ltb'b)



  wog_st : WOrd t => {a : t}
                  -> {set_a : GNESet t a}
                  -> Not (GNELT set_a set_a)
  wog_st (GNELTSingle _ _) impossible
  wog_st (GNELTInd _ _ _ nope) = wog_st nope
  wog_st (GNELTRest {b} _ _ ltbb _ _) = strict {a=b} ltbb



  wog_wf : WOrd t => (set_a : GNESet t a)
                  -> Not (GNELT set_a (Single a))
  wog_wf _ (GNELTSingle _ _) impossible
  wog_wf _ (GNELTRest _ _ _ _ _) impossible



  wog_mp_rest : (w:WOrd t) =>   GNELTRest @{w} ltab ltac ltbc  set_b set_c
                              = GNELTRest @{w} ltab ltac ltbc' set_b set_c
  wog_mp_rest @{w} {ltbc} {ltbc'} = rewrite (mere_prop @{w} ltbc ltbc') in Refl



  implementation [wordgneset] WOrd t => WOrd (GNESet t a) where
    (!<<!) = GNELT

    antisymmetric = wog_as
      where
        wog_as : (w:WOrd t) => {a : t}
                      -> {set1,set2 : GNESet t a}
                      -> GNELT set1 set2
                      -> GNELT set2 set1
                      -> Void
        wog_as (GNELTSingle _ _) (GNELTSingle _ _) impossible
        wog_as (GNELTSingle _ _) (GNELTInd _ _ _ _) impossible
        wog_as (GNELTSingle _ _) (GNELTRest _ _ _ _ _) impossible
        wog_as (GNELTInd ltab set1 set2 lt12) (GNELTInd ltab set2 set1 lt21) = wog_as lt12 lt21
        wog_as (GNELTInd ltab set1 set2 lt12) (GNELTRest {b=b} ltab ltab ltbb set2 set1) =  strict {a=b}ltbb
        wog_as (GNELTRest {b=b} ltab ltab ltbb set_b set_c) (GNELTInd ltab set_c set_b x) = strict {a=b} ltbb
        wog_as (GNELTRest {b=b} {c=c} ltab ltac ltbc set_b set_c) (GNELTRest {b=c} {c=b} ltac ltab ltcb set_c set_b) = antisymmetric {a=b} {b=c} ltbc ltcb


    transitive (GNELTSingle ltab set_b) (GNELTInd ltab set_b set_b' x) = GNELTSingle ltab set_b'
    transitive (GNELTSingle ltab set_b) (GNELTRest ltab ltac ltbc set_b set_c) = GNELTSingle ltac set_c
    transitive (GNELTInd ltab set1 set2 lt12) (GNELTInd ltab set2 set3 lt23) = GNELTInd ltab set1 set3 (transitive @{wordgneset} lt12 lt23)
    transitive (GNELTInd ltab set1 set2 y) (GNELTRest ltab ltac ltbc set2 set_c) = GNELTRest ltab ltac ltbc set1 set_c
    transitive (GNELTRest ltab ltac ltbc set_b set_c) (GNELTInd ltac set_c set2 x) = GNELTRest ltab ltac ltbc set_b set2
    transitive (GNELTRest {a} {b} {c} ltab ltac ltbc set_b set_c) (GNELTRest ltac ltad ltcd set_c set_d) = GNELTRest ltab ltad (transitive {a=b} ltbc ltcd) set_b set_d

    strict = wog_st

    total_order = wog_to
    base = wog_base
    well_founded = wog_wf

    mere_prop (GNELTSingle ltab set_b) (GNELTSingle ltab set_b) = Refl
    mere_prop (GNELTInd ltab set1 set2 y) (GNELTRest {c=b} ltab ltab ltbb set1 set2) = void $ strict {a=b} ltbb
    mere_prop (GNELTRest {c=b} ltab ltab ltbb set_b set_c) (GNELTInd ltab set_b set_c x) = void $ strict {a=b} ltbb
    mere_prop (GNELTRest ltab ltac ltbc set_b set_c) (GNELTRest ltab ltac ltbc' set_b set_c) = wog_mp_rest
    mere_prop (GNELTInd ltab set1 set2 lt12) (GNELTInd ltab set1 set2 lt12') = rewrite (mere_prop @{wordgneset} lt12 lt12') in Refl


